function localization (A,eps)
  [m,n] = size(A)
  D = diag(diag(A))
  B = abs(A - D)
  r = sum(B')
  c = sum(B)
  a = diag(D)
  h=clf; hold on;
  
  lr=min(min(real(a)-3*r))-5;li=min(min(imag(a)-3*r))-5;
  ur=max(max(real(a)+3*r))+5;ui=max(max(imag(a)+3*r))+5;
  g = lr:(ur-lr)/1000:ur; h = lr:(ur-lr)/1000:ur;
  [x,y]=meshgrid(g,h);
  
  f=((abs(x+i*y-a(1))-r(1)).*(abs(x+i*y-a(1))-c(1)))-eps*eps;
  for k=1:n;
    for j=1:n;
    q = ((abs(x+i*y-a(j))-r(j)).*(abs(x+i*y-a(k))-c(k)))-eps*eps;
    f=min(f,q);
    endfor
  endfor
  
  fp=(abs(x+i*y-a(1))-r(1))-eps;
    for j=1:n;
    qp = (abs(x+i*y-a(j))-r(j))-eps;
    fp=min(fp,qp);
    endfor
  f = min(fp,f);
  contour(x, y, f, [0 0],'k','LineWidth',1);
    
  f=[]
  for j=1:length(g)
    for k=1:length(h)
      Z = ((g(j)+i*h(k))*eye(n)-A);
      Z = inv(Z);
      f(k,j) = norm(Z,2)-1/eps;
    endfor
  endfor
  
  contourf(x,y,f, [0 0], 'k', 'LineWidth',1);
  
  plot(real(eig(A)),imag(eig(A)),'rx','MarkerSize',10);
  
  #axis([-100000 100000 -100000 100000])
  axis equal;
  grid on;
endfunction